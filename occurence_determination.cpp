//
// Created by Karol Chęciński on 26.01.2020.
//

#include "segmentation.h"
#include <fstream>
#include <opencv2/opencv.hpp>
#include "occurence_determination.h"

bool is_head(const std::map<std::string, double> &moments) {
    return moments.at("M1") > 0.22 && moments.at("M1") < 0.28 &&
           moments.at("M3") > 0.00066 && moments.at("M3") < 0.0027 &&
           moments.at("M4") > 0.00018 && moments.at("M4") < 0.0067 &&
           moments.at("M7") > 0.012 && moments.at("M7") < 0.017 &&
           moments.at("atr") > 0.45 && moments.at("atr") < 0.54;
}

bool is_body(const std::map<std::string, double> &moments) {
    return moments.at("M1") > 0.27 && moments.at("M1") < 0.34 &&
           moments.at("M3") > 0.0026 && moments.at("M3") < 0.0070 &&
           moments.at("M4") > 0.00019 && moments.at("M4") < 0.00065 &&
           moments.at("M7") > 0.016 && moments.at("M7") < 0.021;
}

bool is_banner(const std::map<std::string, double> &moments) {
    return moments.at("M8") > -0.0000076 && moments.at("M8") < 0.00098 &&
           moments.at("M9") > -0.00049 && moments.at("M9") < 0.0000031 &&
           moments.at("rat") > 2.1 && moments.at("rat") < 3.45;
}

double vectors_angle(const cv::Point &oa, const cv::Point &ob) {
    double oa_len = vector_length(oa);
    double ob_len = vector_length(ob);
    double dot = oa.x * ob.x + oa.y * ob.y;
    return acos(dot / (oa_len * ob_len));
}


std::vector<LadybugTriplet> generate_triplets(const std::vector<Segment> &bodies, const std::vector<Segment> &heads,
                                              const std::vector<Segment> &banners) {
    std::vector<LadybugTriplet> triplets;
    for (const Segment &body: bodies) {
        for (const Segment &head: heads) {
            for (const Segment &banner:banners) {
                triplets.emplace_back(&head, &body, &banner);
            }
        }
    }
    return std::move(triplets);
}

cv::Mat1b draw_triplet(const cv::Size &canvas_size, const LadybugTriplet &triplet) {
    cv::Mat1b image(canvas_size, 0);
    (*std::get<0>(triplet)).draw(image);
    (*std::get<1>(triplet)).draw(image);
    (*std::get<2>(triplet)).draw(image);
    return std::move(image);
}

double sides_ratio(const cv::Point &oa, const cv::Point &ob) {
    double oa_len = vector_length(oa);
    double ob_len = vector_length(ob);
    return oa_len / ob_len;
}

bool is_ladybug(const LadybugTriplet &triplet, bool verbose) {
    cv::Point pos_a = (*std::get<0>(triplet)).rect_centre();
    cv::Point pos_b = (*std::get<2>(triplet)).rect_centre();
    cv::Point pos_o = (*std::get<1>(triplet)).rect_centre();
    cv::Point oa = pos_a - pos_o;
    cv::Point ob = pos_b - pos_o;
    double ratio = sides_ratio(oa, ob);
    double angle = vectors_angle(oa, ob) * 180 / 3.1415;
    double body_side_ratio = (*std::get<1>(triplet)).bounding_rect.width / vector_length(oa);
    if (verbose) {
        std::cout << "Angle: " << angle << std::endl;
        std::cout << "Sides ratio: " << ratio << std::endl;
        std::cout << "Body-side ratio: " << body_side_ratio << std::endl;
    }
    return angle >= 65 && angle <= 85 && ratio >= 0.35 && ratio <= 0.6 && body_side_ratio >= 0.75 &&
           body_side_ratio <= 1.2;
}

double vector_length(const cv::Point &a) {
    return sqrt(a.x * a.x + a.y * a.y);
}

void draw_rect(cv::Mat3b &img, const cv::Rect &rect) {
    for (int i = rect.x; i < rect.x + rect.width; ++i) {
        img[rect.y][i] = {255, 255, 255};
        img[rect.y + rect.height][i] = {255, 255, 255};
    }
    for (int i = rect.y; i < rect.y + rect.height; ++i) {
        img[i][rect.x] = {255, 255, 255};
        img[i][rect.x + rect.width] = {255, 255, 255};
    }
}
