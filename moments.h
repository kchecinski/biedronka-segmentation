//
// Created by Karol Chęciński on 15.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_MOMENTS_H
#define BIEDRONKA_SEGMENTATION_MOMENTS_H

#include <opencv2/opencv.hpp>

std::map<std::string, double> getMoments(const std::vector<cv::Point>& points, const cv::Rect& segment_rect)
{
    std::map<std::string, double> moments;
    double m00 = 0.0, m01 = 0.0, m10 = 0.0, m11 = 0.0, m20 = 0.0, m02 = 0.0, m21 = 0.0, m12 = 0.0, m30 = 0.0, m03 = 0.0;
//    std::cout<<segment_rect<<std::endl;
//    std::cout<<points[0]<<std::endl;
//    std::cout<<points[0].x - segment_rect.x<<" "<<points[0].y - segment_rect.y<<std::endl;
    for (const cv::Point& p: points) {
        int x = p.x - segment_rect.x;
        int y = p.y - segment_rect.y;
//        std::cout<<x<<", "<<y<<std::endl;
        m00 += 1.0;
        m10 += static_cast<double>(x);
        m01 += static_cast<double>(y);
        m11 += static_cast<double>(y) * static_cast<double>(x);
        m20 += std::pow(static_cast<double>(x), 2.0);
        m02 += std::pow(static_cast<double>(y), 2.0);
        m21 += std::pow(static_cast<double>(x), 2.0) * static_cast<double>(y);
        m12 += std::pow(static_cast<double>(y), 2.0) * static_cast<double>(x);
        m30 += std::pow(static_cast<double>(x), 3.0);
        m03 += std::pow(static_cast<double>(y), 3.0);
    }
    double xCent = m10 / m00, yCent = m01 / m00;
    double M11 = m11 - m10 * m01 / m00;
    double M20 = m20 - std::pow(m10, 2.0) / m00;
    double M02 = m02 - std::pow(m01, 2.0) / m00;
    double M21 = m21 - 2.0 * m11 * xCent - m20 * yCent + 2.0 * m01 * std::pow(xCent, 2.0);
    double M12 = m12 - 2.0 * m11 * yCent - m02 * xCent + 2.0 * m10 * std::pow(yCent, 2.0);
    double M30 = m30 - 3.0 * m20 * xCent + 2.0 * m10 * std::pow(xCent, 2.0);
    double M03 = m03 - 3.0 * m02 * yCent + 2.0 * m01 * std::pow(yCent, 2.0);
    moments["atr"] = m00 / (segment_rect.width * segment_rect.height);
    moments["rat"] = static_cast<double>(segment_rect.width) / segment_rect.height;
    moments["M1"] = (M20 + M02) / std::pow(m00, 2.0);
    moments["M2"] = (std::pow(M20 - M02, 2.0) + 4.0 * std::pow(M11, 2.0)) / std::pow(m00, 4.0);
    moments["M3"] = (std::pow(M30 - 3.0 * M12, 2.0) + std::pow(3.0 * M21 - M03, 2.0)) / std::pow(m00, 5.0);
    moments["M4"] = (std::pow(M30 + M12, 2.0) + std::pow(M21 + M03, 2.0)) / std::pow(m00, 5.0);
    moments["M5"] = ((M30 - 3.0 * M12) * (M30 + M12) * (std::pow(M30 + M12, 2.0) - 3.0 * std::pow(M21 + M03, 2.0))
                     + (3.0 * M21 - M03) * (M21 + M03) * (3.0 * std::pow(M30 + M12, 2.0) - std::pow(M21 + M03, 2.0))) / std::pow(m00, 10.0);
    moments["M6"] = ((M20 - M02) * (std::pow(M30 + M12, 2.0) - std::pow(M21 + M03, 2.0)) + 4.0 * M11 * (M30 + M12) * (M21 + M03)) / std::pow(m00, 7.0);
    moments["M7"] = (M20 * M02 - std::pow(M11, 2.0)) / std::pow(m00, 4.0);
    moments["M8"] = (M30 * M12 + M21 * M03 - std::pow(M12, 2.0) - std::pow(M21, 2.0)) / std::pow(m00, 5.0);
    moments["M9"] = (M20 * (M21 * M03 - std::pow(M12, 2.0)) + M02 * (M03 * M12 - std::pow(M21, 2.0)) - M11 * (M30 * M03 - M21 * M12)) / std::pow(m00, 7.0);
    moments["M10"] = (std::pow(M30 * M03 - M12 * M21, 2.0) - 4.0 * (M30 * M12 - std::pow(M21, 2)) * (M03 * M21 - M12)) / std::pow(m00, 10.0);

    return moments;
}
#endif //BIEDRONKA_SEGMENTATION_MOMENTS_H
