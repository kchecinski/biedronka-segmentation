//
// Created by Karol Chęciński on 11.01.2020.
//

#include "histogram.h"

std::vector<int> histogram(const cv::Mat1b &image, int bins) {
    std::vector<int> hist(bins, 0);
    for (int y = 0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            int idx = static_cast<int>(std::round(static_cast<float>(image[y][x]) / 255.0 * bins));
            ++hist[idx];
        }
    }
    return std::move(hist);
}

void print_histogram(const std::vector<int> &hist, int width) {
    int max_val = *std::max_element(hist.cbegin(), hist.cend());
    if (max_val == 0) {
        std::cout<<"Max value 0\n";
        return;
    }
    for(int bin: hist) {
        std::cout << bin << "\t| ";
        int bar_length = (bin * width) / max_val;
        for(int i = 0; i < bar_length; ++i) {
            std::cout << "#";
        }
        std::cout<<"\n";
    }
}

