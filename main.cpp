#include <opencv2/opencv.hpp>

#include <fstream>

#include "image_conversion.h"
#include "filters.h"
#include "histogram.h"
#include "segmentation.h"
#include "morphological.h"
#include "moments.h"
#include "occurence_determination.h"

cv::Mat1b preprocess_image(const cv::Mat3b &image, int th) {
    cv::Mat3f image_hsv = rgb_to_hsv(image);
    std::cout << "th " << th << std::endl;
    std::function<bool(const cv::Vec3f &)> op = [](const cv::Vec3f &v) -> bool {
        return ((v[0] < 60 || v[0] > 340) && v[1] > 0.55 && v[2] > 0.6) ||
               (v[0] > 45 && v[0] < 65 && v[1] > 0.5 && v[2] > 0.76) ||
               (v[1] < 0.15 && v[2] > 0.75);
    };
    cv::Mat1b interest_mask = pointwise_condition(image_hsv, op);
    cv::Mat1b closing_mask = (cv::Mat1b(3, 3) << 0, 255, 0, 255, 255, 255, 0, 255, 0);
    interest_mask = closing(interest_mask, closing_mask);
    cv::Mat3b int_image = cv::Mat3b(image.rows, image.cols);
    for (int y = 0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            int_image[y][x] = interest_mask[y][x] == 255 ? image[y][x] : cv::Vec3b(0, 0, 0);
        }
    }
    static int i = 0;
    cv::imwrite(std::to_string(i) + "interest.bmp", int_image);
    cv::Mat1b image_gray = to_grayscale(int_image);

    cv::Mat1f edges = all_edges(image_gray, 4);

    cv::imwrite(std::to_string(i) + "edges.bmp", edges);
    cv::Mat1b edges_hp = high_pass_filter(edges);
    cv::imwrite(std::to_string(i) + "edges_hp.bmp", edges_hp);

    cv::Mat1b edges_th = thresholding(edges_hp, th);
    cv::imwrite(std::to_string(i) + "th.bmp", edges_th);
    ++i;
    return std::move(edges_th);
}

typedef std::pair<std::string, std::map<std::string, double>> SegmentInfo;


std::vector<SegmentInfo> moments_stats(const std::vector<std::string> &images_names) {
    std::vector<SegmentInfo> segments_info;
    int s = 0;
    std::vector<int> ths = {200, 200, 200, 175, 200};
    auto ths_it = ths.begin();
    for (const std::string &filename : images_names) {
        cv::Mat3b image = cv::imread(filename, 1);
        cv::Mat1b image_prep = preprocess_image(image, *(ths_it++));
        std::vector<Segment> segments = consistent_areas(image_prep, [](float a, float b) -> bool {
            return a == b;
        });
        for (int i = 0; i < segments.size(); ++i, ++s) {
            auto moments = getMoments(segments[i].points, segments[i].bounding_rect);
            std::string segment_name = filename + "_" + std::to_string(i);
            SegmentInfo info;
            info.first = segment_name;
            for (const auto &moment: moments) {
                info.second[moment.first] = moment.second;
            }
            segments_info.push_back(info);
            if (s == 208) {
                cv::Mat1b mat = cv::Mat1b(segments[i].bounding_rect.height + 1, segments[i].bounding_rect.width + 1,
                                          static_cast<uchar>(0));
                for (const cv::Point &p: segments[i].points) {
                    mat[p.y - segments[i].bounding_rect.y][p.x - segments[i].bounding_rect.x] = 255;
                }
                std::cout << mat << std::endl;
            }
            segments[i].save(std::string("segment") + std::to_string(s) + ".bmp");
        }
    }
    return std::move(segments_info);
}


std::vector<Segment> find_ladybug(const cv::Mat3b &image, int th) {
    cv::Mat1b image_prep = preprocess_image(image, th);
    std::vector<Segment> segments = consistent_areas(image_prep, [](float a, float b) -> bool {
        return a == b;
    });
    std::vector<Segment> heads;
    std::vector<Segment> bodies;
    std::vector<Segment> banners;
    for (const auto &segment: segments) {
        auto moments = getMoments(segment.points, segment.bounding_rect);
        if (is_head(moments)) {
            heads.push_back(segment);
        }
        if (is_body(moments)) {
            bodies.push_back(segment);
        }
        if (is_banner(moments)) {
            banners.push_back(segment);
        }
    }
    std::vector<LadybugTriplet> triplets = generate_triplets(bodies, heads, banners);
    std::cout << triplets.size() << " triplets found\n";
    std::vector<Segment> ladybugs;
    for (const auto &triplet: triplets) {
        if (is_ladybug(triplet)) {
            Segment head = *std::get<0>(triplet);
            Segment body = *std::get<1>(triplet);
            Segment banner = *std::get<2>(triplet);
            Segment ladybug({head, body, banner});
            ladybugs.emplace_back(ladybug);
        }
    }
    return std::move(ladybugs);
}


void save_moments(const std::string &out_file, const std::vector<SegmentInfo> &segments_info) {
    std::ofstream file;
    file.open(out_file, std::ios::out);
    file << "name" << "\t";
    for (const auto &moment: segments_info[0].second) {
        file << moment.first << "\t";
    }
    file << "\n";
    for (const auto &st: segments_info) {
        file << st.first << "\t";
        for (const auto &moment: st.second) {
            file << moment.second << "\t";
        }
        file << "\n";
    }
}

int main(int argc, char **argv) {
    std::vector<std::string> names;
    for (int i = 1; i < argc; ++i) {
        names.emplace_back(argv[i]);
    }
    std::vector<int> thresholds = {200, 175, 150, 125, 100};
    int i = 0;
    for (const auto &name: names) {
        std::cout << name << "\n";
        for (int th: thresholds) {
            cv::Mat3b image = cv::imread(name, 1);
            std::vector<Segment> ladybugs = find_ladybug(image, th);
            if (!ladybugs.empty()) {
                for (const Segment &segment: ladybugs) {
                    draw_rect(image, segment.bounding_rect);
                }
                cv::imwrite("results/" + std::to_string(i) + ".bmp", image);
                break;
            }
            std::cout << "Logo wasn't found. Decreasing threshold.\n";
        }
        ++i;
    }

    return 0;
}