//
// Created by Karol Chęciński on 11.01.2020.
//

#include "utils.h"


cv::Mat3f
filter_operation_3f(const cv::Mat3f &im, const std::function<cv::Vec3f(const cv::Mat3f &)> &op, int filter_size) {
    if (filter_size % 2 == 0)
        throw std::runtime_error("Filter size has to be uneven");
    cv::Mat3f result(im.rows, im.cols);
    int pad = (filter_size - 1) / 2;
    for (int x = pad; x < im.cols - pad; ++x) {
        for (int y = pad; y < im.rows - pad; ++y) {
            auto value =op(im(cv::Rect(x-pad, y-pad, filter_size, filter_size)));
            result[y][x] = value;
        }
    }
    return std::move(result);
}

cv::Mat1f
filter_operation_1f(const cv::Mat1f &im, const std::function<float(const cv::Mat1f &)> &op, int filter_size) {
    if (filter_size % 2 == 0)
        throw std::runtime_error("Filter size has to be uneven");
    cv::Mat1f result(im.rows, im.cols);
    int pad = (filter_size - 1) / 2;
    for (int x = pad; x < im.cols - pad; ++x) {
        for (int y = pad; y < im.rows - pad; ++y) {
            auto value =op(im(cv::Rect(x-pad, y-pad, filter_size, filter_size)));
            result[y][x] = value;
        }
    }
    return std::move(result);
}

bool in_image_rect(const cv::Point &pos, const cv::Point &size) {
    return pos.x >= 0 && pos.y >= 0 && pos.x < size.x && pos.y < size.y;
}

std::array<cv::Point, 4> neumann_neighborhood(const cv::Point &point) {
    return std::array<cv::Point, 4>{cv::Point(point.x-1, point.y),
                                    cv::Point(point.x+1, point.y),
                                    cv::Point(point.x, point.y-1),
                                    cv::Point(point.x, point.y+1)};
}

cv::Mat1f op2_in_range(const cv::Mat1f &a, const cv::Mat1f &b, const std::function<float(float, float)> &op, float min,
                       float max) {
    CV_Assert(a.size == b.size);
    CV_Assert(min <= max);
    cv::Mat1f result(a.rows, a.cols);
    for (int x = 0; x < a.cols; ++x) {
        for (int y = 0; y < a.rows; ++y) {
            auto value = op(a[y][x], b[y][x]);
            value = value > min ? value : min;
            value = value < max ? value : max;
            result[y][x] = value;
        }
    }
    return std::move(result);
}

