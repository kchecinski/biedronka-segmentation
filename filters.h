//
// Created by Karol Chęciński on 11.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_FILTERS_H
#define BIEDRONKA_SEGMENTATION_FILTERS_H

#include <opencv2/opencv.hpp>
#include <utility>
#include "utils.h"


cv::Vec3f max_value(const cv::Mat3f& image, int ch);

cv::Vec3f min_value(const cv::Mat3f& image, int ch);

cv::Vec3b median_value(const cv::Mat3b &image);
float median_value_1f(const cv::Mat1f &image);

cv::Vec3f conv_value(const cv::Mat1f& mask, const cv::Mat3f& field);
float conv_value(const cv::Mat1f &mask, const cv::Mat1f &field);

cv::Mat3f median_filter(const cv::Mat3f &image, int filter_size);
cv::Mat1f median_filter_1f(const cv::Mat1f &image, int filter_size);

cv::Mat3f max_filter(const cv::Mat3f& image, int filter_size, int ch);

cv::Mat3f min_filter(const cv::Mat3f& image, int filter_size, int ch);

cv::Mat3f conv_filter(const cv::Mat3f& image, const cv::Mat1f& mask);
cv::Mat1f conv_filter(const cv::Mat1f &image, const cv::Mat1f &mask);

cv::Mat3f mean_filter(const cv::Mat3f &image, int filter_size);

cv::Mat1f vertical_edge_detection(const cv::Mat1f &image, float c);

cv::Mat1f horizontal_edge_detection(const cv::Mat1f &, int c);

cv::Mat1f diagonal_a_edges_detection(const cv::Mat1f &image, int c);

cv::Mat1f diagonal_b_edges_detection(const cv::Mat1f &image, int c);

cv::Mat1f all_edges(const cv::Mat1f &image, int c);

cv::Mat1f high_pass_filter(const cv::Mat1f &image);

#endif //BIEDRONKA_SEGMENTATION_FILTERS_H
