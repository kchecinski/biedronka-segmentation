//
// Created by Karol Chęciński on 12.01.2020.
//

#include "segmentation.h"
#include "utils.h"

cv::Mat1b thresholding(const cv::Mat1b &image, uchar threshold) {
    const std::function<uchar(const uchar &)> &op =
            [threshold](const uchar &val) -> uchar { return val > threshold ? 255 : 0; };
    return std::move(pointwise_operation(image, op));
}


std::vector<Segment> consistent_areas(const cv::Mat1f &image, const std::function<bool(float, float)> &crit) {
    cv::Mat1b areas = cv::Mat(image.rows, image.cols, CV_8UC1, cv::Scalar(0));
    std::vector<Segment> segments;
    cv::Point size(image.cols, image.rows);
    for (int y = 0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            if (areas[y][x] == 0) {
                Segment curr_segment;
                std::queue<cv::Point> points;
                areas[y][x] = 255;
                curr_segment.add_point(cv::Point(x, y));
                points.emplace(x, y);
                while (!points.empty()) {
                    cv::Point curr_point = points.front();
                    points.pop();
//                    std::cout<<curr_point<<"\t"<<points.size()<<std::endl;
                    std::array<cv::Point, 4> neighbors = neumann_neighborhood(curr_point);
                    for (const auto &neighbor: neighbors) {
                        if (in_image_rect(neighbor, size) && areas[neighbor.y][neighbor.x] == 0
                            && crit(image[curr_point.y][curr_point.x],
                                    image[neighbor.y][neighbor.x])) {
//                            std::cout<<neighbor<<std::endl;
                            areas[neighbor.y][neighbor.x] = 255;
                            points.push(neighbor);
                            curr_segment.add_point(neighbor);
                        }
                    }
                }
                if (curr_segment.area() > 50 && curr_segment.bounding_rect.width > 15 &&
                    curr_segment.bounding_rect.height > 15 && curr_segment.bounding_rect.width < 400 &&
                    curr_segment.bounding_rect.height < 200) {
                    segments.push_back(curr_segment);
                }
            }
        }
    }
    return std::move(segments);
}

cv::Mat1b pointwise_condition(const cv::Mat3f &image, std::function<bool(const cv::Vec3f &)> &cond) {
    std::function<cv::Vec<uchar, 1>(const cv::Vec3f &)> op = [&cond](const cv::Vec3f &v) -> cv::Vec<uchar, 1> {
        return cond(v) ? 255 : 0;
    };
    return std::move(pointwise_operation(image, op));
}

void Segment::update_rect(const cv::Point &p) {
    min_x = p.x < min_x ? p.x : min_x;
    max_x = p.x > max_x ? p.x : max_x;
    min_y = p.y < min_y ? p.y : min_y;
    max_y = p.y > max_y ? p.y : max_y;
    bounding_rect.x = min_x;
    bounding_rect.y = min_y;
    bounding_rect.width = max_x - min_x + 1;
    bounding_rect.height = max_y - min_y + 1;
}

void Segment::add_point(const cv::Point &p) {
    points.push_back(p);
    update_rect(p);
}

cv::Mat1b Segment::get_mask() const {
    cv::Mat1b result = cv::Mat1b(bounding_rect.height + 1, bounding_rect.width + 1, static_cast<uchar>(0));
    for (const cv::Point &point : points) {
        result[point.y - bounding_rect.y][point.x - bounding_rect.x] = 255;
    }
    return std::move(result);
}

int Segment::area() const {
    return points.size();
}

void Segment::save(const std::string &filename) {
    cv::Mat1b mat = get_mask();
    cv::imwrite(filename, mat);
}

cv::Point Segment::rect_centre() const {
    return cv::Point(bounding_rect.x + bounding_rect.width / 2, bounding_rect.y + bounding_rect.height / 2);
}

void Segment::draw(cv::Mat1b &img) const {
    assert(in_size(img.size()));
    for (const cv::Point &point: points) {
        img[point.y][point.x] = 255;
    }
}

bool Segment::in_size(const cv::Size &size) const {
    return size.width > bounding_rect.x + bounding_rect.width && size.height > bounding_rect.y + bounding_rect.height;
}

Segment::Segment(const std::vector<Segment>& subsegments) {
    for (const auto& segment: subsegments) {
        for (const auto& point: segment.points) {
            add_point(point);
        }
    }
}
