//
// Created by Karol Chęciński on 12.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_SEGMENTATION_H
#define BIEDRONKA_SEGMENTATION_SEGMENTATION_H

#include <opencv2/opencv.hpp>
#include <bits/stdc++.h>

struct Segment {
    cv::Rect bounding_rect = {-1, -1, 0, 0};
    std::vector<cv::Point> points;

    Segment() = default;

    explicit Segment(const std::vector<Segment> &subsegments);

    void update_rect(const cv::Point &p);

    void add_point(const cv::Point &p);

    cv::Mat1b get_mask() const;

    void save(const std::string &filename);

    inline int area() const;

    cv::Point rect_centre() const;

    void draw(cv::Mat1b &img) const;

    bool in_size(const cv::Size &size) const;

private:
    int min_x = INT_MAX, min_y = INT_MAX;
    int max_x = INT_MIN, max_y = INT_MIN;
};

cv::Mat1b thresholding(const cv::Mat1b &image, uchar threshold);

cv::Mat1b pointwise_condition(const cv::Mat3f &image, std::function<bool(const cv::Vec3f &)> &cond);

std::vector<Segment> consistent_areas(const cv::Mat1f &image, const std::function<bool(float, float)> &crit);

cv::Mat1b consistent_areas_hue(const cv::Mat1f &image, float threshold);


#endif //BIEDRONKA_SEGMENTATION_SEGMENTATION_H
