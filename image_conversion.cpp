//
// Created by Karol Chęciński on 11.01.2020.
//
#include <cmath>
#include "image_conversion.h"
#include "utils.h"

cv::Vec3f rgb_to_hsv_px(const cv::Vec3f& pixel) {
    cv::Vec3f result;
    uchar max = pixel_max(pixel);
    uchar min = pixel_min(pixel);
    uchar d = max - min;
    result[2] = static_cast<float>(pixel_max(pixel)) / 255;
    if (d == 0)
        result[0] = 0;
    else if (static_cast<float>(max) == pixel[2])
        result[0] = 60 * (std::fmod(static_cast<float>(pixel[1] - pixel[0]) / static_cast<float>(d), 6));
    else if (static_cast<float>(max) == pixel[1])
        result[0] = 60 * (static_cast<float>(pixel[0] - pixel[2]) / static_cast<float>(d) + 2);
    else if (static_cast<float>(max) == pixel[0])
        result[0] = 60 * (static_cast<float>(pixel[2] - pixel[1]) / static_cast<float>(d) + 4);
    result[1] = max == 0 ? 0 : static_cast<float>(d) / static_cast<float>(max);
    if (result[0] < 0)
        result[0] += 360;
    return result;
}

cv::Vec3b hsv_to_rgb_px(const cv::Vec3f &pixel) {
    float c = pixel[2] * pixel[1];
    float x = c * (1.0 - std::abs((std::fmod(pixel[0] / 60, 2) - 1)));
    float m = pixel[2] - c;
    cv::Vec3f pp;
    if (pixel[0] >= 0 && pixel[0] < 60)
        pp = cv::Vec3f(0, x, c);
    else if (pixel[0] >= 60 && pixel[0] < 120)
        pp = cv::Vec3f(0, c, x);
    else if (pixel[0] >= 120 && pixel[0] < 180)
        pp = cv::Vec3f(x, c, 0);
    else if (pixel[0] >= 180 && pixel[0] < 240)
        pp = cv::Vec3f(c, x, 0);
    else if (pixel[0] >= 240 && pixel[0] < 300)
        pp = cv::Vec3f(c, 0, x);
    else if (pixel[0] >= 300 && pixel[0] < 360)
        pp = cv::Vec3f(x, 0, c);
    cv::Vec3b result;
    for (int i = 0; i < 3; ++i) {
        result[i] = (pp[i] + m) * 255;
    }
    return result;
}

cv::Mat3f rgb_to_hsv(const cv::Mat3f &im) {
    static const std::function<cv::Vec3f(const cv::Vec3f&)>& op = rgb_to_hsv_px;
    cv::Mat3f result = pointwise_operation(im, op);
    return std::move(result);
}

cv::Mat3b hsv_to_rgb(const cv::Mat3f &im_hsv) {
    static const std::function<cv::Vec3f(const cv::Vec3f&)>& op = hsv_to_rgb_px;
    cv::Mat3b result = pointwise_operation(im_hsv, op);
    return std::move(result);
}

cv::Mat3f char2float(const cv::Mat3b &im) {
    cv::Mat3f result = cv::Mat3f(im.rows, im.cols);
    for (int y = 0; y < im.rows; ++y) {
        for (int x = 0; x < im.cols; ++x) {
            result[y][x] = static_cast<cv::Vec3f>(im[y][x]);
        }
    }
    return std::move(result);
}

cv::Mat3b float2char(const cv::Mat3f &im) {
    cv::Mat3b result = cv::Mat3b(im.rows, im.cols);
    for (int y = 0; y < im.rows; ++y) {
        for (int x = 0; x < im.cols; ++x) {
            result[y][x] = static_cast<cv::Vec3b>(im[y][x]);
        }
    }
    return std::move(result);
}

cv::Mat1b to_grayscale(const cv::Mat3b &im) {
    const std::function<ushort(const cv::Vec3b&)>& op = [](const cv::Vec3b& vec) -> ushort {
        return vec[0] / 3 + vec[1] / 3 + vec[2] / 3;
    };
    return pointwise_operation(im, op);
}

