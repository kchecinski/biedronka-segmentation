//
// Created by Karol Chęciński on 11.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_HISTOGRAM_H
#define BIEDRONKA_SEGMENTATION_HISTOGRAM_H

#include <opencv2/opencv.hpp>
std::vector<int> histogram(const cv::Mat1b& image, int bins);

void print_histogram(const std::vector<int>& hist, int height);



#endif //BIEDRONKA_SEGMENTATION_HISTOGRAM_H
