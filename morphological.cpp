//
// Created by Karol Chęciński on 14.01.2020.
//

#include "morphological.h"
#include "utils.h"

cv::Mat1b erosion(const cv::Mat1b &im, cv::Mat1b &mask) {
    int mask_size = mask.rows;
    std::function<float(const cv::Mat1f&)> op = [&mask](const cv::Mat1f& field) -> float {
        return intersect(mask, 255 - field) ? 0 : 255;
    };
    return std::move(filter_operation_1f(im, op, mask_size));
}

cv::Mat1b dilation(const cv::Mat1b &im, cv::Mat1b &mask) {
    int mask_size = mask.rows;
    std::function<float(const cv::Mat1f&)> op = [&mask](const cv::Mat1f& field) -> float {
        return intersect(mask, field) ? 255 : 0;
    };
    return std::move(filter_operation_1f(im, op, mask_size));
}

cv::Mat1b opening(const cv::Mat1b &im, cv::Mat1b &mask) {
    cv::Mat1b eroded = erosion(im, mask);
    return std::move(dilation(eroded, mask));
}

cv::Mat1b closing(const cv::Mat1b &im, cv::Mat1b &mask) {
    cv::Mat1b dilated = dilation(im, mask);
    return std::move(erosion(dilated, mask));
}

bool intersect(const cv::Mat1b &a, const cv::Mat1b &b) {
    CV_Assert(a.size == b.size);
    for (int y = 0; y < a.rows; ++y) {
        for (int x = 0; x < a.cols; ++x) {
            if (a[y][x] == 255 && b[y][x] == 255)
                return true;
        }
    }
    return false;
}
