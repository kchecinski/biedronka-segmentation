//
// Created by Karol Chęciński on 11.01.2020.
//

#include "filters.h"

cv::Vec3f max_value(const cv::Mat3f &image, int ch) {
    cv::Vec3f curr = image[0][0];
    for (int y=0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            if (image[y][x][ch] > curr[ch]) {
                curr = image[y][x];
            }
        }
    }
    return curr;
}

cv::Vec3f min_value(const cv::Mat3f &image, int ch) {
    cv::Vec3f curr = image[0][0];
    for (int y=0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            if (image[y][x][ch] < curr[ch]) {
                curr = image[y][x];
            }
        }
    }
    return curr;
}

cv::Vec3b median_value(const cv::Mat3b &image) {
    typedef std::pair<uchar, cv::Vec3b> GrayVecPair;
    std::vector<GrayVecPair> values;
    for (int y=0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            uchar gray_value = image[y][x][0] / 3 + image[y][x][1] / 3 + image[y][x][2] / 3;
            values.emplace_back(gray_value, image[y][x]);
        }
    }
    int mid = (image.rows * image.cols) / 2;
    std::partial_sort(values.begin(), values.begin() + (mid+1), values.end(),
                      [](const GrayVecPair& a, const GrayVecPair& b) -> bool {return a.first > b.first;});
    return values[mid].second;
}

float median_value_1f(const cv::Mat1f &image) {
    std::vector<float> values;
    for (int y=0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            values.push_back(image[y][x]);
        }
    }
    int mid = (image.rows * image.cols) / 2;
    std::partial_sort(values.begin(), values.begin() + (mid+1), values.end());
    return values[mid];
}

cv::Vec3f conv_value(const cv::Mat1f &mask, const cv::Mat3f &field) {
    if (mask.rows != field.rows || mask.cols != field.cols) {
        throw std::runtime_error("Mask and field have to have same size");
    }
    cv::Vec3f value = cv::Vec3f(0, 0, 0);
    for (int y=0; y < field.rows; ++y) {
        for (int x = 0; x < field.cols; ++x) {
            value += field[y][x] * mask[y][x];
        }
    }
    return value;
}

float conv_value(const cv::Mat1f &mask, const cv::Mat1f &field) {
    if (mask.rows != field.rows || mask.cols != field.cols) {
        throw std::runtime_error("Mask and field have to have same size");
    }
    float value = 0.0;
    for (int y=0; y < field.rows; ++y) {
        for (int x = 0; x < field.cols; ++x) {
            auto temp = field[y][x] * mask[y][x];
            value += temp;
        }
    }
    value = value > 255 ? 255 : (value < 0 ? -value : value);
    return value;
}

cv::Mat3f median_filter(const cv::Mat3f &image, int filter_size) {
    static std::function<cv::Vec3f(const cv::Mat3f&)> op = [](const cv::Mat3f& field) -> cv::Vec3f {
        return median_value(field);
    };
    return std::move(filter_operation_3f(image, op, filter_size));
}

cv::Mat1f median_filter_1f(const cv::Mat1f &image, int filter_size) {
    static std::function<float(const cv::Mat1f&)> op = [](const cv::Mat1f& field) -> float {
        return median_value_1f(field);
    };
    return std::move(filter_operation_1f(image, op, filter_size));
}

cv::Mat3f max_filter(const cv::Mat3f &image, int filter_size, int ch) {
    static std::function<cv::Vec3f(const cv::Mat3f&)> op = [ch](const cv::Mat3f& field) -> cv::Vec3f {
        return max_value(field, ch);
    };
    return std::move(filter_operation_3f(image, op, filter_size));
}

cv::Mat3f min_filter(const cv::Mat3f &image, int filter_size, int ch) {
    static std::function<cv::Vec3f(const cv::Mat3f&)> op = [ch](const cv::Mat3f& field) -> cv::Vec3f {
        return min_value(field, ch);
    };
    return std::move(filter_operation_3f(image, op, filter_size));
}

cv::Mat3f conv_filter(const cv::Mat3f &image, const cv::Mat1f &mask) {
    if (mask.rows != mask.cols) {
        throw std::runtime_error("Mask has to be squared");
    }
    int filter_size = mask.rows;
    static std::function<cv::Vec3f(const cv::Mat3f&)> op = [&mask](const cv::Mat_<cv::Vec3f>& field) -> cv::Vec3f {
        return conv_value(mask, field);
    };
    return std::move(filter_operation_3f(image, op, filter_size));
}

cv::Mat1f conv_filter(const cv::Mat1f &image, const cv::Mat1f &mask) {
    if (mask.rows != mask.cols) {
        throw std::runtime_error("Mask has to be squared");
    }
    int filter_size = mask.rows;
    std::function<float(const cv::Mat1f&)> op = [&mask](const cv::Mat1f& field) -> float {
        return conv_value(mask, field);
    };
    return std::move(filter_operation_1f(image, op, filter_size));
}

cv::Mat3f mean_filter(const cv::Mat3f &image, int filter_size) {
    static cv::Mat1f mask = cv::Mat1f(filter_size, filter_size, 1.0/(filter_size*filter_size));
    return std::move(conv_filter(image, mask));
}

cv::Mat1f vertical_edge_detection(const cv::Mat1f &image, float c) {
    static cv::Mat1f mask = (cv::Mat1f(3,3) << 1, c, 1, 0, 0, 0, -1, -c, -1);
    cv::Mat1f result = conv_filter(image, mask);
    return std::move(result);
}

cv::Mat1f horizontal_edge_detection(const cv::Mat1f &image, int c) {
    static cv::Mat1f mask = (cv::Mat1f(3,3) << 1, 0, -1, c, 0, -c, 1, 0, -1);
    cv::Mat1f result = conv_filter(image, mask);
    return std::move(result);
}

cv::Mat1f diagonal_a_edges_detection(const cv::Mat1f &image, int c) {
    static cv::Mat1f mask = (cv::Mat1f(3,3) << c, 1, 0, 1, 0, -1, 0, -1, -c);
    cv::Mat1f result = conv_filter(image, mask);
    return std::move(result);
}

cv::Mat1f diagonal_b_edges_detection(const cv::Mat1f &image, int c) {
    static cv::Mat1f mask = (cv::Mat1f(3, 3)<< 0, 1, c, -1, 0, 1, -c, -1, 0);
    cv::Mat1f result = conv_filter(image, mask);
    return std::move(result);
}

cv::Mat1f high_pass_filter(const cv::Mat1f &image) {
    static cv::Mat1f mask = (cv::Mat1f(3, 3) << 0, -1, 0, -1, 5, -1, 0, -1, 0);
    static std::function<float(const float&)> op = [](const float& v) -> float {return v > 255 ? 255 : (v < 0 ? 0 : v);};
    cv::Mat1f result = conv_filter(image, mask);
    result = pointwise_operation(result, op);
    return std::move(result);
}

cv::Mat1f all_edges(const cv::Mat1f &image, int c) {
    cv::Mat1f edges_v = vertical_edge_detection(image, c);
    cv::Mat1f edges_h = horizontal_edge_detection(image, c);
    cv::Mat1f edges_d1 = diagonal_a_edges_detection(image, c);
    cv::Mat1f edges_d2 = diagonal_b_edges_detection(image, c);

    cv::Mat1f edges_o = op2_in_range(edges_h, edges_v, [](float a, float b) -> float{return (a + b)/2;}, 0, 255);
    cv::Mat1f edges_d = op2_in_range(edges_d1, edges_d2, [](float a, float b) -> float{return (a + b)/2;}, 0, 255);
    cv::Mat1f edges = op2_in_range(edges_o, edges_d, [](float a, float b) -> float{return (a + b)/2;}, 0, 255);

    return std::move(edges);
}
