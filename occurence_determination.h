//
// Created by Karol Chęciński on 26.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_OCCURENCE_DETERMINATION_H
#define BIEDRONKA_SEGMENTATION_OCCURENCE_DETERMINATION_H

typedef std::tuple<const Segment *, const Segment *, const Segment *> LadybugTriplet;

bool is_head(const std::map<std::string, double> &moments);

bool is_body(const std::map<std::string, double> &moments);

bool is_banner(const std::map<std::string, double> &moments);

bool is_ladybug(const LadybugTriplet& triplet, bool verbose=false);

double vector_length(const cv::Point &a);

double vectors_angle(const cv::Point &oa, const cv::Point &ob);

double sides_ratio(const cv::Point &oa, const cv::Point &ob);

std::vector<LadybugTriplet> generate_triplets(const std::vector<Segment> &bodies, const std::vector<Segment> &heads,
                                              const std::vector<Segment> &banners);

cv::Mat1b draw_triplet(const cv::Size& canvas_size, const LadybugTriplet &triplet);

void draw_rect(cv::Mat3b& img, const cv::Rect& rect);


#endif //BIEDRONKA_SEGMENTATION_OCCURENCE_DETERMINATION_H
