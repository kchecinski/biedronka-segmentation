import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def normalize(segments):
    mins = []
    maxs = []
    for col in segments.columns[1:]:
        mins.append(min(segments[col]))
        segments[col] = segments[col] - mins[-1]
        maxs.append(max(segments[col]))
        segments[col] = segments[col] / maxs[-1]
    return mins, maxs


def get_scatter_plot_pos(segments):
    xs = []
    ys = []
    for i, col in enumerate(segments.columns[1:]):
        xs += [i + 1] * len(segments[col])
        ys += list(segments[col])
    return np.array(xs), np.array(ys)


def analyse(segments, interesting_segments):
    for col in segments.columns[1:]:
        all_min = min(segments[col])
        all_max = max(segments[col])
        int_min = min(interesting_segments[col])
        int_max = max(interesting_segments[col])
        # int_min *= 0.95
        # int_max *= 1.05
        all = segments.shape[0]
        in_range = segments[segments[col] < int_max]
        in_range = in_range[in_range[col] > int_min].shape[0]
        print(F"{col}:\tmin:\t{int_min}\tmax:\t{int_max}\t({in_range/all:.3f})")
        print(F"Part of whole range: {(int_max - int_min)/(all_max - all_min):.4f}")
        print(F"in range: {in_range}/{all}\t({in_range/all:.5f})")


def main():
    moments = pd.read_csv('../segments2.txt', sep='\t')
    # mins, maxs = normalize(moments)
    head_segs = [5, 70, 168, 200, 256]
    body_segs = [6, 73, 174, 208, 264]
    banner_segs = [7, 72, 173, 202, 262]
    types = [head_segs, body_segs, banner_segs]
    xs, ys = get_scatter_plot_pos(moments)

    for segs_type in types:
        type_moments = moments.iloc[segs_type]
        type_xs, type_ys = get_scatter_plot_pos(type_moments)
        analyse(moments, type_moments)
        plt.scatter(xs, ys)
        plt.scatter(type_xs, type_ys)
        plt.show()

        print()


if __name__ == "__main__":
    main()