//
// Created by Karol Chęciński on 11.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_IMAGE_CONVERSION_H
#define BIEDRONKA_SEGMENTATION_IMAGE_CONVERSION_H

#include <opencv2/opencv.hpp>

cv::Vec3f rgb_to_hsv_px(const cv::Vec3f &pixel);
cv::Vec3b hsv_to_rgb_px(const cv::Vec3f &pixel);

cv::Mat3f rgb_to_hsv(const cv::Mat3f& im);
cv::Mat3b hsv_to_rgb(const cv::Mat3f &im_hsv);

cv::Mat3f char2float(const cv::Mat3b &im);
cv::Mat3b float2char(const cv::Mat3f &im);

cv::Mat1b to_grayscale(const cv::Mat3b &im);

#endif //BIEDRONKA_SEGMENTATION_IMAGE_CONVERSION_H
