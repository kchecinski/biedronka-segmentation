//
// Created by Karol Chęciński on 14.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_MORPHOLOGICAL_H
#define BIEDRONKA_SEGMENTATION_MORPHOLOGICAL_H

#include <opencv2/opencv.hpp>

bool intersect(const cv::Mat1b& a, const cv::Mat1b& b);

cv::Mat1b erosion(const cv::Mat1b& im, cv::Mat1b& mask);
cv::Mat1b dilation(const cv::Mat1b& im, cv::Mat1b& mask);
cv::Mat1b opening(const cv::Mat1b& im, cv::Mat1b& mask);
cv::Mat1b closing(const cv::Mat1b& im, cv::Mat1b& mask);

#endif //BIEDRONKA_SEGMENTATION_MORPHOLOGICAL_H
