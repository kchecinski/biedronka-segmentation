//
// Created by Karol Chęciński on 11.01.2020.
//

#ifndef BIEDRONKA_SEGMENTATION_UTILS_H
#define BIEDRONKA_SEGMENTATION_UTILS_H
#include <opencv2/opencv.hpp>
#include <stdexcept>

template <class T, int cn>
T pixel_max(const cv::Vec<T, cn> &px) {
    T result = px[0];
    for (int i = 1; i < cn; ++i) {
        result = result > px[i] ? result : px[i];
    }
    return result;
}

template <class T, int cn>
T pixel_min(const cv::Vec<T, cn> &px) {
    T result = px[0];
    for (int i = 1; i < cn; ++i) {
        result = result < px[i] ? result : px[i];
    }
    return result;
}

template<class T, class U>
cv::Mat_<T> pointwise_operation(const cv::Mat_<U> &im, const std::function<T(const U&)>& op) {
    cv::Mat_<T> result(im.rows, im.cols);
    for (int x = 0; x < im.cols; ++x) {
        for (int y = 0; y < im.rows; ++y) {
            result[y][x] = op(im[y][x]);
        }
    }
    return std::move(result);
}

cv::Mat3f filter_operation_3f(const cv::Mat3f &im, const std::function<cv::Vec3f(const cv::Mat3f&)>& op,
                              int filter_size);

cv::Mat1f filter_operation_1f(const cv::Mat1f &im, const std::function<float(const cv::Mat1f &)> &op, int filter_size);

bool in_image_rect(const cv::Point &pos, const cv::Point &size);

std::array<cv::Point, 4> neumann_neighborhood(const cv::Point& point);

cv::Mat1f op2_in_range(const cv::Mat1f& a, const cv::Mat1f& b,
        const std::function<float(float, float)>& op, float min, float max);
#endif //BIEDRONKA_SEGMENTATION_UTILS_H
